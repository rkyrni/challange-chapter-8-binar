const Overlay = ({ onClick }) => {
  return (
    <div
      onClick={onClick}
      className="absolute top-0 left-0 right-0 bottom-0 z-10 bg-slate-500 opacity-25"
    ></div>
  );
};

export default Overlay;
