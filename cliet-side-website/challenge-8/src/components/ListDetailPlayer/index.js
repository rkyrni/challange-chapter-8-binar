const ListDetailPlayer = ({ title, value }) => {
  return (
    <tr>
      <th className="text-start text-black px-3">{title}</th>
      <th className="text-black">:</th>
      <td className="text-slate-600 px-5">{value}</td>
    </tr>
  );
};

export default ListDetailPlayer;
