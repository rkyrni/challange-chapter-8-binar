import React from "react";

const Input = ({ type, placeholder, name, value, onChange, className }) => {
  return (
    <div className="form-control w-full max-w-xs p-4">
      <input
        type={type}
        placeholder={placeholder}
        name={name}
        value={value}
        onChange={onChange}
        className={`input input-bordered w-full max-w-xs bg-slate-100 ${className}`}
      />
    </div>
  );
};

export default Input;
