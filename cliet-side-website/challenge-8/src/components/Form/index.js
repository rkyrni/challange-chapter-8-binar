import Input from "../Input";

const Form = ({ onClickCancel, onSubmit, input, onChange }) => {
  return (
    <div className="relative">
      <form onSubmit={onSubmit} action="POST" className={`text-black`}>
        <Input
          type={`text`}
          required
          name={`username`}
          value={input.username}
          onChange={onChange}
          placeholder={`username`}
        />
        <Input
          type={`email`}
          required
          name={`email`}
          value={input.email}
          onChange={onChange}
          placeholder={`email`}
        />
        <Input
          type={`password`}
          required
          name={`password`}
          value={input.password}
          onChange={onChange}
          placeholder={`password(min 6 char)`}
        />
        <div className="absolute right-6">
          <button
            disabled={
              input.password.length < 6 ||
              input.username.length === 0 ||
              input.email.length === 0
            }
            className="btn bg-blue-400 text-white0 border-none hover:bg-blue-500 disabled:text-black disabled:line-through text-white"
          >
            Submit
          </button>
        </div>
      </form>
      <div className="pl-5">
        <button
          onClick={onClickCancel}
          className="btn bg-red-500 border-none hover:bg-red-600 text-white"
        >
          Cancel
        </button>
      </div>
    </div>
  );
};

export default Form;
