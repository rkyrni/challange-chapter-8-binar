import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { DataContext } from "../../Context";
import { ReactComponent as ButtonSidebarIcon } from "../../assets/icons/chevron.svg";
import { ReactComponent as SearchIcon } from "../../assets/icons/searchIcon.svg";
import Input from "../Input";
import Alert from "../Alert";

const Sidebar = () => {
  const [isAlertInput, setIsAlertInput] = useState(false);
  const [filterPlayersInput, setFilterPlayersInput] = useState({
    username: "",
    email: "",
    experience: "",
    lvl: "",
  });
  const {
    setisDataFilterPlayer,
    setDataFilterPlayer,
    sideBarCollapse,
    setSideBarCollapse,
    triggerPlayerDetail,
    setTriggerPlayerDetail,
    setPlayerDetail,
  } = useContext(DataContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (isAlertInput) {
      setTimeout(() => {
        setIsAlertInput(false);
      }, 3500);
    }
  }, [isAlertInput]);

  const inputFilterHandler = (e) => {
    setFilterPlayersInput({
      ...filterPlayersInput,
      [e.target.name]: e.target.value,
    });
  };

  const handlerSubmitFilter = async (e) => {
    e.preventDefault();
    // navigate("/");
    try {
      const { username, email, experience, lvl } = filterPlayersInput;
      if (!username && !email && !experience && !lvl) {
        setIsAlertInput(true);
        navigate("/");
        setTriggerPlayerDetail(!triggerPlayerDetail);
        return;
      }

      const data = await axios.get(
        `http://localhost:4000/api/v1/players?${
          username ? `username=${username}&` : ``
        }${email ? `email=${email}&` : ``}${
          experience ? `experience=${experience}&` : ``
        }${lvl ? `lvl=${lvl}` : ``}`
      );
      setTriggerPlayerDetail(!triggerPlayerDetail);
      if (data.data.data.length !== 0) {
        navigate(`/${data.data.data[0].id}`);
      } else {
        navigate("/");
        setPlayerDetail(undefined);
      }
      setDataFilterPlayer(data.data.data);
      setFilterPlayersInput({
        username: "",
        email: "",
        experience: "",
        lvl: "",
      });
      setisDataFilterPlayer(true);
      setIsAlertInput(false);
    } catch (error) {
      alert(error);
    }
  };

  const SidebarBtnCollapse = () => {
    return (
      <button
        className={`${
          sideBarCollapse ? "rotate-180" : ""
        } absolute bg-black duration-700 text-yellow-600 border-4 border-yellow-600 rounded-full p-2 -right-4 top-8`}
        onClick={() => {
          setSideBarCollapse(!sideBarCollapse);
        }}
      >
        <ButtonSidebarIcon width="12px" />
      </button>
    );
  };

  return (
    <div className="flex flex-col relative">
      <SidebarBtnCollapse />
      <div className="pt-28 flex justify-center">
        <span className="text-slate-600 font-bold text-lg flex self-center">
          <SearchIcon width={`27px`} />
          <span className={`${sideBarCollapse ? "hidden" : ""}`}>
            &nbsp;&nbsp;Filter Players
          </span>
        </span>
      </div>
      <form
        className={`${sideBarCollapse ? "hidden" : ""}`}
        onSubmit={handlerSubmitFilter}
      >
        <Input
          type={`text`}
          name="username"
          className={`${
            isAlertInput ? "border border-red-500 rounded" : ""
          } text-slate-700`}
          placeholder={`username`}
          onChange={inputFilterHandler}
          value={filterPlayersInput.username}
        />
        <Input
          type={`email`}
          name="email"
          className={`${
            isAlertInput ? "border border-red-500 rounded" : ""
          } text-slate-700`}
          placeholder={`email`}
          onChange={inputFilterHandler}
          value={filterPlayersInput.email}
        />
        <Input
          type={`number`}
          name="experience"
          className={`${
            isAlertInput ? "border border-red-500 rounded" : ""
          } text-slate-700`}
          placeholder={`experience`}
          onChange={inputFilterHandler}
          value={filterPlayersInput.experience}
        />
        <Input
          type={`number`}
          name="lvl"
          className={`${
            isAlertInput ? "border border-red-500 rounded" : ""
          } text-slate-700`}
          placeholder={`lvl`}
          onChange={inputFilterHandler}
          value={filterPlayersInput.lvl}
        />
        <div className="p-3">
          <button className="btn w-full bg-green-400 border-none hover:bg-green-600 text-white">
            Submit
          </button>
        </div>
      </form>
      {isAlertInput && (
        <div className={`p-5 ${sideBarCollapse ? "hidden" : ""}`}>
          <Alert value={`fill in at least one input`} alertType={`warning`} />
        </div>
      )}
    </div>
  );
};

export default Sidebar;
