import { createContext, useState } from "react";

export const DataContext = createContext();

const StateManagement = (props) => {
  const [sideBarCollapse, setSideBarCollapse] = useState(false);
  const [dataFIlterPlayer, setDataFilterPlayer] = useState([]);
  const [isDataFilterPlayer, setisDataFilterPlayer] = useState(false);
  const [playerDetail, setPlayerDetail] = useState(undefined);
  const [triggerPlayerDetail, setTriggerPlayerDetail] = useState(false);

  return (
    <DataContext.Provider
      value={{
        sideBarCollapse,
        setSideBarCollapse,
        dataFIlterPlayer,
        setDataFilterPlayer,
        isDataFilterPlayer,
        setisDataFilterPlayer,
        playerDetail,
        setPlayerDetail,
        triggerPlayerDetail,
        setTriggerPlayerDetail,
      }}
    >
      {props.children}
    </DataContext.Provider>
  );
};

export default StateManagement;
