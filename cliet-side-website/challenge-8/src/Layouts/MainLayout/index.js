import { useContext } from "react";
import Sidebar from "../../components/Sidebar";
import { DataContext } from "../../Context";

const MainLayout = ({ element }) => {
  const { sideBarCollapse } = useContext(DataContext);

  return (
    <div className="flex gap-4 bg-slate-100">
      <div
        className={`${
          sideBarCollapse ? `w-16` : `w-1/5`
        } h-screen bg-white shadow-lg duration-500`}
      >
        <Sidebar />
      </div>
      <div className="w-full h-screen p-10 flex">
        <div className="bg-white w-full rounded-lg shadow-lg">{element}</div>
      </div>
    </div>
  );
};

export default MainLayout;
