import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import StateManagement from "../Context";
import MainLayout from "../Layouts/MainLayout";
import Main from "../pages/Main";

const Router = () => {
  return (
    <BrowserRouter>
      <StateManagement>
        <Routes>
          <Route path="/" element={<MainLayout element={<Main />} />} />
          <Route path="/:id" element={<MainLayout element={<Main />} />} />
        </Routes>
      </StateManagement>
    </BrowserRouter>
  );
};

export default Router;
