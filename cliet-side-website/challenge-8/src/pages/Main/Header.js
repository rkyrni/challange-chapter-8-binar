import React from "react";
import Alert from "../../components/Alert";
import { ReactComponent as BackIcon } from "../../assets/icons/backIcon.svg";

const Header = ({ isDataFilterPlayer, handlerCancel, flashMessage }) => {
  return (
    <div className="w-1/2 flex justify-start">
      {isDataFilterPlayer && (
        <>
          <button className="" onClick={handlerCancel}>
            <BackIcon width="40px" color="black" />
          </button>
        </>
      )}
      {flashMessage && (
        <>
          <Alert
            value={flashMessage}
            alertType={`success`}
            className={`bg-green-500 text-slate-700`}
          />
        </>
      )}
    </div>
  );
};

export default Header;
