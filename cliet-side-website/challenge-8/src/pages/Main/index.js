import React, { useContext, useEffect, useState } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import { DataContext } from "../../Context";
import { ReactComponent as DeletekIcon } from "../../assets/icons/deleteIcon.svg";
import { ReactComponent as PencilIcon } from "../../assets/icons/pencilIcon.svg";
import ListDetailPlayer from "../../components/ListDetailPlayer";
import Overlay from "../../components/Overlay";
import Header from "./Header";
import DataListPlayer from "./DataListPlayer";
import Modals from "./Modals";

const Main = () => {
  const {
    playerDetail,
    setPlayerDetail,
    isDataFilterPlayer,
    setisDataFilterPlayer,
    setDataFilterPlayer,
    triggerPlayerDetail,
    setTriggerPlayerDetail,
  } = useContext(DataContext);

  const [fetchData, setFetchData] = useState(true);
  const [flashMessage, setFlashMessage] = useState(false);
  const [isEditExperience, setIsEditExperience] = useState(false);
  const [inputEditExperience, setinputEditExperience] = useState("");
  const [openModalFormEdit, setOpenModalFormEdit] = useState(false);
  const [openModalFormCreate, setOpenModalFormCreate] = useState(false);
  const [modalDelete, setModalDelete] = useState(false);
  const [inputCreateEditPlayer, setInputCreateEditPlayer] = useState({
    username: "",
    email: "",
    password: "",
  });

  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    const fetchById = async () => {
      const player = await axios.get(
        `http://localhost:4000/api/v1/players/${id}`
      );
      setPlayerDetail(player.data.data);
    };

    if (id) {
      fetchById();
    }
  }, [id, setPlayerDetail, triggerPlayerDetail, setTriggerPlayerDetail]);

  useEffect(() => {
    if (flashMessage) {
      setTimeout(() => {
        setFlashMessage(false);
      }, 3500);
    }
  }, [flashMessage]);

  const handlerSubmitExperience = async (e) => {
    e.preventDefault();
    try {
      await axios.post(`http://localhost:4000/api/v1/players/exp/${id}`, {
        exp: inputEditExperience,
      });
      setIsEditExperience(false);
      setinputEditExperience("");
      setTriggerPlayerDetail(!triggerPlayerDetail);
      setFlashMessage(`Update Experience ${playerDetail.username} Success!`);
    } catch (error) {
      alert(error);
    }
  };

  const handlerCancel = () => {
    setisDataFilterPlayer(false);
    setPlayerDetail(undefined);
    navigate("/");
  };

  const handleEditExperience = () => {
    setIsEditExperience(true);
    setinputEditExperience(playerDetail.experience);
  };

  const handlerButtonEdit = () => {
    setInputCreateEditPlayer({
      username: playerDetail.username,
      email: playerDetail.email,
      password: "",
    });
    setOpenModalFormEdit(true);
  };

  return (
    <div className="flex flex-col h-full">
      {isEditExperience && (
        <Overlay onClick={() => setIsEditExperience(false)} />
      )}
      <Modals
        inputCreateEditPlayer={inputCreateEditPlayer}
        setOpenModalFormCreate={setOpenModalFormCreate}
        setInputCreateEditPlayer={setInputCreateEditPlayer}
        setFlashMessage={setFlashMessage}
        setFetchData={setFetchData}
        setisDataFilterPlayer={setisDataFilterPlayer}
        setDataFilterPlayer={setDataFilterPlayer}
        setTriggerPlayerDetail={setTriggerPlayerDetail}
        triggerPlayerDetail={triggerPlayerDetail}
        playerDetail={playerDetail}
        setOpenModalFormEdit={setOpenModalFormEdit}
        setModalDelete={setModalDelete}
        setPlayerDetail={setPlayerDetail}
        openModalFormCreate={openModalFormCreate}
        openModalFormEdit={openModalFormEdit}
        modalDelete={modalDelete}
      />
      <div className="w-full shadow-lg justify-end py-3 flex gap-10 px-5">
        <Header
          isDataFilterPlayer={isDataFilterPlayer}
          handlerCancel={handlerCancel}
          flashMessage={flashMessage}
        />
        <div className="flex justify-end w-1/2">
          <button
            onClick={() => setOpenModalFormCreate(true)}
            className="btn bg-green-400 border-none hover:bg-green-600 text-white"
          >
            Create Player
          </button>
        </div>
      </div>
      <div className="flex p-10 gap-4 h-full overflow-hidden">
        <div className="w-1/2  overflow-y-auto">
          <table className="w-full">
            <tbody>
              <DataListPlayer
                fetchData={fetchData}
                setFetchData={setFetchData}
              />
            </tbody>
          </table>
        </div>
        <div className="w-1/2 border border-dashed border-slate-300 rounded">
          {playerDetail !== undefined && (
            <>
              <div className="flex flex-col justify-center pt-10 gap-8">
                <table className="flex items-stretch self-center border border-dashed border-slate-400 rounded-lg p-5">
                  <tbody>
                    <ListDetailPlayer
                      title={`Username`}
                      value={playerDetail.username}
                    />
                    <ListDetailPlayer
                      title={`Email`}
                      value={playerDetail.email}
                    />
                    <ListDetailPlayer
                      title={`Experience`}
                      value={
                        <form
                          action="POST"
                          onSubmit={handlerSubmitExperience}
                          className="flex justify-between"
                        >
                          {isEditExperience ? (
                            <input
                              className="pl-1 border border-slate-500 z-40 rounded"
                              type="number"
                              name="inputEditExperience"
                              onChange={(e) =>
                                setinputEditExperience(e.target.value)
                              }
                              id="experience"
                              value={inputEditExperience}
                            />
                          ) : (
                            <>{playerDetail.experience}</>
                          )}

                          <label
                            onClick={handleEditExperience}
                            htmlFor="experience"
                            className="pt-1 pl-1 hover:cursor-pointer"
                          >
                            <PencilIcon width={`15px`} />
                          </label>
                        </form>
                      }
                    />
                    <ListDetailPlayer title={`Lvl`} value={playerDetail.lvl} />
                  </tbody>
                </table>
                <div className="flex justify-end pr-32 gap-6">
                  <button
                    onClick={() => setModalDelete(true)}
                    className="btn bg-red-500 border-none text-black font-bold hover:bg-red-600"
                  >
                    <DeletekIcon />
                  </button>
                  <button
                    onClick={handlerButtonEdit}
                    className="btn bg-blue-600 border-none text-white font-bold hover:bg-blue-800"
                  >
                    Edit
                  </button>
                </div>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Main;
