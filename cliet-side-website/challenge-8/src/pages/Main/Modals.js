import axios from "axios";
import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import Form from "../../components/Form";
import Modal from "../../components/Modal";

const Modals = (props) => {
  const {
    inputCreateEditPlayer,
    setOpenModalFormCreate,
    setInputCreateEditPlayer,
    setFlashMessage,
    setFetchData,
    setisDataFilterPlayer,
    setDataFilterPlayer,
    setTriggerPlayerDetail,
    triggerPlayerDetail,
    playerDetail,
    setOpenModalFormEdit,
    setModalDelete,
    setPlayerDetail,
    openModalFormCreate,
    openModalFormEdit,
    modalDelete,
  } = props;
  const defaultInputCreateEditPlayer = {
    username: "",
    email: "",
    password: "",
  };

  const navigate = useNavigate();
  const { id } = useParams();

  const handlerSubmitCreatePlayer = async (e) => {
    e.preventDefault();
    try {
      const newPlayer = await axios.post(
        `http://localhost:4000/api/v1/players`,
        inputCreateEditPlayer
      );
      setOpenModalFormCreate(false);
      setInputCreateEditPlayer(defaultInputCreateEditPlayer);
      setFlashMessage(`Create Data ${newPlayer.data.data.username} Success!`);
      setFetchData(true);
      navigate(`/${newPlayer.data.data.id}`);
      setisDataFilterPlayer(false);
      setDataFilterPlayer([]);
    } catch (error) {
      if (error.response.status === 500) {
        alert(`Player Data is Already`);
      }
    }
  };

  const handleChangeInputCreate = (e) => {
    setInputCreateEditPlayer({
      ...inputCreateEditPlayer,
      [e.target.name]: e.target.value,
    });
  };

  const HandlerSubmitEditPlayer = async (e) => {
    e.preventDefault();
    try {
      await axios.put(
        `http://localhost:4000/api/v1/players/${id}`,
        inputCreateEditPlayer
      );
      setTriggerPlayerDetail(!triggerPlayerDetail);
      setOpenModalFormCreate(false);
      setInputCreateEditPlayer(defaultInputCreateEditPlayer);
      setFlashMessage(`berhasil edit data ${playerDetail.username}`);
      setFetchData(true);
      setOpenModalFormEdit(false);
      navigate(`/${id}`);
      setisDataFilterPlayer(false);
      setDataFilterPlayer([]);
    } catch (error) {
      alert(error);
    }
  };

  const handlerDeletePlayer = async (e) => {
    e.preventDefault();
    try {
      await axios.delete(`http://localhost:4000/api/v1/players/${id}`);
      setFetchData(true);
      setModalDelete(false);
      setPlayerDetail(undefined);
      navigate("/");
      setDataFilterPlayer([]);
      setisDataFilterPlayer(false);
      setFlashMessage("Berhasil hapus player!");
    } catch (error) {
      alert(error);
    }
  };

  const handlerCloseModalCreatePlayer = () => {
    setOpenModalFormCreate(false);
    setOpenModalFormEdit(false);
    setInputCreateEditPlayer(defaultInputCreateEditPlayer);
  };

  return (
    <>
      <Modal
        title={`Form Create Player`}
        element={
          <Form
            onClickCancel={handlerCloseModalCreatePlayer}
            onSubmit={handlerSubmitCreatePlayer}
            input={inputCreateEditPlayer}
            onChange={handleChangeInputCreate}
          />
        }
        isOpen={openModalFormCreate}
        onClose={handlerCloseModalCreatePlayer}
      />
      <Modal
        title={`Form Edit Player`}
        element={
          <Form
            onClickCancel={handlerCloseModalCreatePlayer}
            onSubmit={HandlerSubmitEditPlayer}
            input={inputCreateEditPlayer}
            onChange={handleChangeInputCreate}
          />
        }
        isOpen={openModalFormEdit}
        onClose={handlerCloseModalCreatePlayer}
      />
      <Modal
        title={`Delete Player ${playerDetail?.username}`}
        element={
          <div className="flex justify-evenly">
            <button
              onClick={() => setModalDelete(false)}
              className="btn btn-error"
            >
              cancel
            </button>
            <button onClick={handlerDeletePlayer} className="btn btn-warning">
              delete
            </button>
          </div>
        }
        isOpen={modalDelete}
        onClose={() => setModalDelete(false)}
      />
    </>
  );
};

export default Modals;
