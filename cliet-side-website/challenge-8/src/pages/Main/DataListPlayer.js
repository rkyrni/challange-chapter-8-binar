import axios from "axios";
import React, { useContext, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Alert from "../../components/Alert";
import { DataContext } from "../../Context";

const DataListPlayer = ({ fetchData, setFetchData }) => {
  const [dataPlayers, setDataPlayers] = useState(null);
  const { isDataFilterPlayer, dataFIlterPlayer } = useContext(DataContext);
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    const fetch = async () => {
      try {
        const data = await axios.get("http://localhost:4000/api/v1/players");
        setDataPlayers(data.data.data);
      } catch (error) {
        console.log(error);
      }
    };

    if (fetchData) {
      fetch();
      setFetchData(false);
    }
  }, [fetchData, setFetchData]);

  const handlerClickPlayer = (id) => {
    navigate(`/${id}`);
  };

  const ListItemPlayer = ({ idPlayer, usernamePlayer }) => {
    return (
      <tr>
        <td>
          <button
            onClick={() => handlerClickPlayer(idPlayer)}
            className={`${
              Number(id) === idPlayer
                ? "bg-green-400 text-white hover:bg-green-500"
                : "bg-slate-900 hover:bg-black"
            } p-2 w-full rounded border-none`}
          >
            {usernamePlayer}
          </button>
        </td>
      </tr>
    );
  };

  const DataView = () => {
    if (isDataFilterPlayer) {
      if (dataFIlterPlayer.length === 0) {
        return (
          <tr>
            <td>
              <Alert value={`No Data Player`} alertType={`warning`} />
            </td>
          </tr>
        );
      }
      return (
        <>
          {dataFIlterPlayer.map((el, index) => {
            return (
              <ListItemPlayer
                key={index}
                idPlayer={el.id}
                usernamePlayer={el.username}
              />
            );
          })}
        </>
      );
    } else {
      return (
        <>
          {dataPlayers.map((el, index) => {
            return (
              <ListItemPlayer
                key={index}
                idPlayer={el.id}
                usernamePlayer={el.username}
              />
            );
          })}
        </>
      );
    }
  };

  return (
    <>
      {dataPlayers === null ? (
        <tr>
          <td>
            <Alert value={`No Data Player`} alertType={`warning`} />
          </td>
        </tr>
      ) : (
        <>
          <DataView />
        </>
      )}
    </>
  );
};

export default DataListPlayer;
